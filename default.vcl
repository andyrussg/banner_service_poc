vcl 4.0;
import header;
import std;

backend default {
	.host = "mediawiki-web";
	.port = "8080";
}

backend banners {
	.host = "banners";
	.port = "3000";
}

sub vcl_backend_response {
	# Turn on ESI
	set beresp.do_esi = true;

	# **** Dev setup only: no caching of backend responses
	set beresp.ttl = 0s;
	set beresp.grace = 0s;
}

sub vcl_recv {
	if( req.url ~ "banners-service" ) {
		set req.backend_hint = banners;
	}

	if (req.esi_level != 0) {
		set req.http.X-ESI-Parent = req_top.url;
	}
}
