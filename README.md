# banner_service_POC

1. Set up a local Docker MediaWiki instance.
2. Install CentralNotice on that instance.
3. Copy docker-compose.override.yml, Varnish-Dockerfile and banner-service to the base MediaWiki directory.
4. Check out this CentralNotice change: https://gerrit.wikimedia.org/r/c/mediawiki/extensions/CentralNotice/+/767047
